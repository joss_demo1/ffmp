@echo off

set ROOT1=%~dp0\build\install\bin
set ROOT2=%~dp0\cmake-build-debug
if EXIST %ROOT1% (
set PYTHONPATH=%ROOT1%;%PYTHONPATH%
set QT_QPA_PLATFORM_PLUGIN_PATH=%ROOT1%\..\plugins\platforms
) ELSE if EXIST %ROOT2% (
set PYTHONPATH=%ROOT2%;%PYTHONPATH%
set QT_QPA_PLATFORM_PLUGIN_PATH=%ROOT2%\..\plugins\platforms
)
set QT_DEBUG_PLUGINS=1

set TEST_IMG="test/seq/test1.jpg"
set TEST_CMD="import ffmp; r=ffmp.img('%TEST_IMG%');print(f'Result: {r}')"
echo Launching: %TEST_CMD%
::%USERPROFILE%\Vcpkg\installed\x64-windows\tools\python3\python.exe -c %TEST_CMD%
py -c %TEST_CMD%
