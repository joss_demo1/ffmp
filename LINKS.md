https://www.qtcentre.org/threads/70155-OpenEXR-to-QImage


Python args parsing:

  * https://github.com/Yelp/pygear/blob/master/client.c

OpenCV face detection and video streaming:

  * https://lembergsolutions.com/blog/how-process-live-video-stream-using-ffmpeg-and-opencv

NanoVDB:

  * https://benjamin.ahlbrand.me/nanovdb

Non-standard Development library Python3:

  * https://stackoverflow.com/questions/69767573/find-the-interpreter-and-development-components-of-a-custom-built-python-via-cma

CMake debugging:

  * https://hsf-training.github.io/hsf-training-cmake-webpage/08-debugging/index.html
  * cmake -P cmake_install.cmake --trace-source=cmake_install.cmake