==========================================

30.nov.2020:

  * Python location is set in CMakeLists.txt
  * TODO: bug with Qt platform plugins for Windows
  * Add OpenVDB, OIIO, OCIO, Qt6 to CmakeLists.txt
  * Fix cmake verbosity
  * Add Qt widget code
  * Add OpenVDB dumper code
  * Change FFMPEG to a package
  * Fix Qt6 windeployqt problem
  * TODO: Fix OpenCV, FFMPEG deployment as well


10.nov.2020:

  * Add setup.py / cmake / vcpkg support
  * Sort out Python interpreter location problem, now it can be built for any interpreter with standard layout.
