@echo off
cls

set "PY0=C:\Program Files\Side Effects Software\Houdini 19.5.403\python39"
set PY1=%PY0%;%PY0%\Scripts;%PY0%\Library\bin
set PATH=%PY1%;%FAR3%;%PATH%
::set "PYTHONPATH=C:\Projects\Maya_api;C:\Projects\Rbh\rph_app;%PYTHONPATH%"

set bd=%~dp0build
if EXIST %bd% echo Removing %bd% && rd /s /q %bd%
"%PY0%\python.exe" setup.py build_ext