@echo off

set QT_DEBUG_PLUGINS=1
echo %QT_DEBUG_PLUGINS%

set ROOT=%~dp0\cmake-build-debug
if EXIST %ROOT% (
echo Using CLion
) ELSE (
echo Using Cmake
set ROOT=%~dp0\build\install\bin
)
set PYTHONPATH=%ROOT%;%PYTHONPATH%
set QT_QPA_PLATFORM_PLUGIN_PATH=%ROOT%\..\plugins\platforms
echo %QT_QPA_PLATFORM_PLUGIN_PATH%

::%USERPROFILE%\Vcpkg\installed\x64-windows\tools\python3\python.exe -c %TEST_CMD%
FOR /F "tokens=*" %%g IN ('where python') do (SET PY=%%g)
echo Using Python: "%PY%"

set TEST_CMD="import ffmp;r=ffmp.qt();print(f'Result: {r}')"
echo Launching: %TEST_CMD%
%PY% -c %TEST_CMD%
