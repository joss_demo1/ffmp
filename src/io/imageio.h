#pragma once

#include "headers.h"
#include "image/timage.h"

using namespace OIIO;


namespace NImage
{
	std::vector<unsigned char> Load(std::string fpn)
	{
		std::vector<unsigned char> buf;
		auto inp = ImageInput::open(fpn);
		if (!inp)
			return buf;
		const ImageSpec &spec = inp->spec();
		int xres = spec.width;
		int yres = spec.height;
		int channels = spec.nchannels;
		auto img_bytes = xres * yres * channels;
		buf.clear();
		buf.resize(img_bytes);
		inp->read_image(TypeDesc::UINT8, &buf[0]);
		inp->close();
		return buf;
	}
}