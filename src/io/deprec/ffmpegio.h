#pragma once

#include <iostream>

extern "C" {
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <libavutil/imgutils.h>
#include <libavutil/opt.h>
}

namespace NEncode
{
	inline int Test(std::string ouf = "./out.mp4", int rx = 1920, int ry = 1080, int fps = 24)
	/*
	 *
	 */
	{
		auto filename = ouf.c_str();
		AVOutputFormat *fmt;
		AVFormatContext *fctx;
		AVCodecContext *cctx;
		AVStream *st;

		//av_register_all();
		//avcodec_register_all();

		//auto detect the output format from the name
		fmt = av_guess_format(NULL, filename, NULL);
		if (!fmt) {
			std::cout << "Error av_guess_format()" << std::endl;
			system("pause");
			exit(1);
		}
		if (avformat_alloc_output_context2(&fctx, fmt, NULL, filename) < 0) {
			std::cout << "Error avformat_alloc_output_context2()" << std::endl;
			system("pause");
			exit(1);
		}


		//stream creation + parameters
		st = avformat_new_stream(fctx, 0);
		if (!st) {
			std::cout << "Error avformat_new_stream()" << std::endl;
			system("pause");
			exit(1);
		}

		st->codecpar->codec_id = fmt->video_codec;
		st->codecpar->codec_type = AVMEDIA_TYPE_VIDEO;
		st->codecpar->width = rx;
		st->codecpar->height = ry;
		st->time_base.num = 1;
		st->time_base.den = fps;

		AVCodec *pCodec = avcodec_find_encoder(st->codecpar->codec_id);
		if (!pCodec) {
			std::cout << "Error avcodec_find_encoder()" << std::endl;
			system("pause");
			exit(1);
		}

		cctx = avcodec_alloc_context3(pCodec);
		if (!cctx) {
			std::cout << "Error avcodec_alloc_context3()" << std::endl;
			system("pause");
			exit(1);
		}

		avcodec_parameters_to_context(cctx, st->codecpar);
		cctx->bit_rate = 400000;
		cctx->width = rx;
		cctx->height = ry;
		cctx->time_base.num = 1;
		cctx->time_base.den = fps;
		cctx->gop_size = 12;
		cctx->pix_fmt = AV_PIX_FMT_YUV420P;
		if (st->codecpar->codec_id == AV_CODEC_ID_H264)
			av_opt_set(cctx->priv_data, "preset", "ultrafast", 0);
		if (fctx->oformat->flags & AVFMT_GLOBALHEADER)
			cctx->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;
		avcodec_parameters_from_context(st->codecpar, cctx);
		av_dump_format(fctx, 0, filename, 1);

		//OPEN FILE + WRITE HEADER
		if (avcodec_open2(cctx, pCodec, NULL) < 0) {
			std::cout << "Error avcodec_open2()" << std::endl;
			system("pause");
			exit(1);
		}
		if (!(fmt->flags & AVFMT_NOFILE)) {
			if (avio_open(&fctx->pb, filename, AVIO_FLAG_WRITE) < 0) {
				std::cout << "Error avio_open()" << std::endl;
				system("pause");
				exit(1);
			}
		}
		if (avformat_write_header(fctx, NULL) < 0) {
			std::cout << "Error avformat_write_header()" << std::endl;
			system("pause");
			exit(1);
		}

		//CREATE DUMMY VIDEO
		AVFrame *frame = av_frame_alloc();
		frame->format = cctx->pix_fmt;
		frame->width = cctx->width;
		frame->height = cctx->height;
		av_image_alloc(frame->data, frame->linesize, cctx->width, cctx->height, cctx->pix_fmt, 32);

		AVPacket pkt;
		double video_pts = 0;
		for (int i = 0; i < 250; i++) {
			video_pts = (double) cctx->time_base.num / cctx->time_base.den * 90 * i;

			for (int y = 0; y < cctx->height; y++) {
				for (int x = 0; x < cctx->width; x++) {
					frame->data[0][y * frame->linesize[0] + x] = x + y + i * 3;
					if (y < cctx->height / 2 && x < cctx->width / 2) {
						/* Cb and Cr */
						frame->data[1][y * frame->linesize[1] + x] = 128 + y + i * 2;
						frame->data[2][y * frame->linesize[2] + x] = 64 + x + i * 5;
					}
				}
			}

			av_init_packet(&pkt);
			pkt.flags |= AV_PKT_FLAG_KEY;
			pkt.pts = frame->pts = video_pts;
			pkt.data = NULL;
			pkt.size = 0;
			pkt.stream_index = st->index;

			if (avcodec_send_frame(cctx, frame) < 0) {
				std::cout << "Error avcodec_send_frame()" << std::endl;
				exit(1);
			}
			if (avcodec_receive_packet(cctx, &pkt) == 0) {
				//std::cout << "Write frame " << to_string((int) pkt.pts) << std::endl;
				av_interleaved_write_frame(fctx, &pkt);
				av_packet_unref(&pkt);
			}
		}

		//DELAYED FRAMES
		for (;;) {
			avcodec_send_frame(cctx, NULL);
			if (avcodec_receive_packet(cctx, &pkt) == 0) {
				//std::cout << "-Write frame " << to_string((int)pkt.pts) << std::endl;
				av_interleaved_write_frame(fctx, &pkt);
				av_packet_unref(&pkt);
			} else {
				break;
			}
		}

		//FINISH
		av_write_trailer(fctx);
		if (!(fmt->flags & AVFMT_NOFILE)) {
			if (avio_close(fctx->pb) < 0) {
				std::cout << "Error avio_close()" << std::endl;
				system("pause");
				exit(1);
			}
		}
		av_frame_free(&frame);
		avcodec_free_context(&cctx);
		avformat_free_context(fctx);
		return 0;
	}

}
