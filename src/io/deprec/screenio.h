#pragma once

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <cstring>
#include <math.h>
#include <string.h>

#define __STDC_CONSTANT_MACROS

//FFMPEG LIBRARIES
extern "C" {
#include "libavcodec/avcodec.h"
#include "libavcodec/avfft.h"
#include "libavdevice/avdevice.h"
#include "libavfilter/avfilter.h"
#include "libavfilter/buffersink.h"
#include "libavfilter/buffersrc.h"
#include "libavformat/avformat.h"
#include "libavformat/avio.h"

// libav resample
#include "libavutil/opt.h"
#include "libavutil/common.h"
#include "libavutil/channel_layout.h"
#include "libavutil/imgutils.h"
#include "libavutil/mathematics.h"
#include "libavutil/samplefmt.h"
#include "libavutil/time.h"
#include "libavutil/opt.h"
#include "libavutil/pixdesc.h"
#include "libavutil/file.h"
// lib swresample
#include "libswscale/swscale.h"

}

namespace NScreen
{

	class ScreenRecorder
	{
	private:
		AVInputFormat *pAVInputFormat;
		AVOutputFormat *output_format;

		AVCodecContext *pAVCodecContext;

		AVFormatContext *pAVFormatContext;

		AVFrame *pAVFrame;
		AVFrame *outFrame;

		AVCodec *pAVCodec;
		AVCodec *outAVCodec;

		AVPacket *pAVPacket;

		AVDictionary *options;

		AVOutputFormat *outAVOutputFormat;
		AVFormatContext *outAVFormatContext;
		AVCodecContext *outAVCodecContext;

		AVStream *video_st;
		AVFrame *outAVFrame;

		const char *dev_name;
		const char *output_file;

		double video_pts;

		int out_size;
		int codec_id;
		int value;
		int VideoStreamIndx;

	public:

		inline ScreenRecorder()
		{
			/* initialize the resources*/
			//av_register_all();
			//avcodec_register_all();
			avdevice_register_all();
			std::cout << "\nall required functions are registered successfully";
		}

		~ScreenRecorder()
		{
			/* uninitialize the resources */
			avformat_close_input(&pAVFormatContext);
			if (!pAVFormatContext) {
				std::cout << "\nfile closed sucessfully";
			} else {
				std::cout << "\nunable to close the file";
				exit(1);
			}

			avformat_free_context(pAVFormatContext);
			if (!pAVFormatContext) {
				std::cout << "\navformat free successfully";
			} else {
				std::cout << "\nunable to free avformat context";
				exit(1);
			}

		}

		int openCamera()
		{
			/* function to initiate communication with display library */
			/* establishing the connection between camera or screen through its respective folder */
			value = 0;
			options = NULL;
			pAVFormatContext = NULL;

			pAVFormatContext = avformat_alloc_context();//Allocate an AVFormatContext.
			/*

			X11 video input device.
			To enable this input device during configuration you need libxcb installed on your system. It will be automatically detected during configuration.
			This device allows one to capture a region of an X11 display.
			refer : https://www.ffmpeg.org/ffmpeg-devices.html#x11grab
			*/
			/* current below is for screen recording. to connect with camera use v4l2 as a input parameter for av_find_input_format */
			pAVInputFormat = av_find_input_format("x11grab");
			value = avformat_open_input(&pAVFormatContext, ":0.0+10,250", pAVInputFormat, NULL);
			if (value != 0) {
				std::cout << "\nerror in opening input device";
				exit(1);
			}

			/* set frame per second */
			value = av_dict_set(&options, "framerate", "30", 0);
			if (value < 0) {
				std::cout << "\nerror in setting dictionary value";
				exit(1);
			}

			value = av_dict_set(&options, "preset", "medium", 0);
			if (value < 0) {
				std::cout << "\nerror in setting preset values";
				exit(1);
			}

			//	value = avformat_find_stream_info(pAVFormatContext,NULL);
			if (value < 0) {
				std::cout << "\nunable to find the stream information";
				exit(1);
			}

			VideoStreamIndx = -1;

			/* find the first video stream index . Also there is an API available to do the below operations */
			for (int i = 0; i < pAVFormatContext->nb_streams; i++) // find video stream posistion/index.
			{
				if (pAVFormatContext->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_VIDEO) {
					VideoStreamIndx = i;
					break;
				}

			}

			if (VideoStreamIndx == -1) {
				std::cout << "\nunable to find the video stream index. (-1)";
				exit(1);
			}

			// assign pAVFormatContext to VideoStreamIndx
			pAVCodecContext = pAVFormatContext->streams[VideoStreamIndx]->codec;

			pAVCodec = avcodec_find_decoder(pAVCodecContext->codec_id);
			if (pAVCodec == NULL) {
				std::cout << "\nunable to find the decoder";
				exit(1);
			}

			value = avcodec_open2(pAVCodecContext, pAVCodec, NULL);//Initialize the AVCodecContext to use the given AVCodec.
			if (value < 0) {
				std::cout << "\nunable to open the av codec";
				exit(1);
			}
			return 0;
		}

		int init_outputfile()
		{
			/* initialize the video output file and its properties  */
			outAVFormatContext = NULL;
			value = 0;
			output_file = "../media/output.mp4";

			avformat_alloc_output_context2(&outAVFormatContext, NULL, NULL, output_file);
			if (!outAVFormatContext) {
				std::cout << "\nerror in allocating av format output context";
				exit(1);
			}

			/* Returns the output format in the list of registered output formats which best matches the provided parameters, or returns NULL if there is no match. */
			output_format = av_guess_format(NULL, output_file, NULL);
			if (!output_format) {
				std::cout << "\nerror in guessing the video format. try with correct format";
				exit(1);
			}

			video_st = avformat_new_stream(outAVFormatContext, NULL);
			if (!video_st) {
				std::cout << "\nerror in creating a av format new stream";
				exit(1);
			}

			outAVCodecContext = avcodec_alloc_context3(outAVCodec);
			if (!outAVCodecContext) {
				std::cout << "\nerror in allocating the codec contexts";
				exit(1);
			}

			/* set property of the video file */
			outAVCodecContext = video_st->codec;
			outAVCodecContext->codec_id = AV_CODEC_ID_MPEG4;// AV_CODEC_ID_MPEG4; // AV_CODEC_ID_H264 // AV_CODEC_ID_MPEG1VIDEO
			outAVCodecContext->codec_type = AVMEDIA_TYPE_VIDEO;
			outAVCodecContext->pix_fmt = AV_PIX_FMT_YUV420P;
			outAVCodecContext->bit_rate = 400000; // 2500000
			outAVCodecContext->width = 1920;
			outAVCodecContext->height = 1080;
			outAVCodecContext->gop_size = 3;
			outAVCodecContext->max_b_frames = 2;
			outAVCodecContext->time_base.num = 1;
			outAVCodecContext->time_base.den = 30; // 15fps

			if (codec_id == AV_CODEC_ID_H264) {
				av_opt_set(outAVCodecContext->priv_data, "preset", "slow", 0);
			}

			outAVCodec = avcodec_find_encoder(AV_CODEC_ID_MPEG4);
			if (!outAVCodec) {
				std::cout << "\nerror in finding the av codecs. try again with correct codec";
				exit(1);
			}

			/* Some container formats (like MP4) require global headers to be present
			   Mark the encoder so that it behaves accordingly. */

			if (outAVFormatContext->oformat->flags & AVFMT_GLOBALHEADER) {
				outAVCodecContext->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;
			}

			value = avcodec_open2(outAVCodecContext, outAVCodec, NULL);
			if (value < 0) {
				std::cout << "\nerror in opening the avcodec";
				exit(1);
			}

			/* create empty video file */
			if (!(outAVFormatContext->flags & AVFMT_NOFILE)) {
				if (avio_open2(&outAVFormatContext->pb, output_file, AVIO_FLAG_WRITE, NULL, NULL) < 0) {
					std::cout << "\nerror in creating the video file";
					exit(1);
				}
			}

			if (!outAVFormatContext->nb_streams) {
				std::cout << "\noutput file dose not contain any stream";
				exit(1);
			}

			/* imp: mp4 container or some advanced container file required header information*/
			value = avformat_write_header(outAVFormatContext, &options);
			if (value < 0) {
				std::cout << "\nerror in writing the header context";
				exit(1);
			}

			/*
			// uncomment here to view the complete video file informations
			std::cout<<"\n\nOutput file information :\n\n";
			av_dump_format(outAVFormatContext , 0 ,output_file ,1);
			*/
			return 0;
		}


		int CaptureVideoFrames()
		{
			/* function to capture and store data in frames by allocating required memory and auto deallocating the memory.   */
			int flag;
			int frameFinished;//when you decode a single packet, you still don't have information enough to have a frame [depending on the type of codec, some of them //you do], when you decode a GROUP of packets that represents a frame, then you have a picture! that's why frameFinished will let //you know you decoded enough to have a frame.

			int frame_index = 0;
			value = 0;

			pAVPacket = (AVPacket *) av_malloc(sizeof(AVPacket));
			av_init_packet(pAVPacket);

			pAVFrame = av_frame_alloc();
			if (!pAVFrame) {
				std::cout << "\nunable to release the avframe resources";
				exit(1);
			}

			outFrame = av_frame_alloc();//Allocate an AVFrame and set its fields to default values.
			if (!outFrame) {
				std::cout << "\nunable to release the avframe resources for outframe";
				exit(1);
			}

			int video_outbuf_size;
			int nbytes = av_image_get_buffer_size(outAVCodecContext->pix_fmt, outAVCodecContext->width, outAVCodecContext->height, 32);
			uint8_t *video_outbuf = (uint8_t *) av_malloc(nbytes);
			if (video_outbuf == NULL) {
				std::cout << "\nunable to allocate memory";
				exit(1);
			}

			// Setup the data pointers and linesizes based on the specified image parameters and the provided array.
			value = av_image_fill_arrays(outFrame->data, outFrame->linesize, video_outbuf, AV_PIX_FMT_YUV420P, outAVCodecContext->width, outAVCodecContext->height, 1); // returns : the size in bytes required for src
			if (value < 0) {
				std::cout << "\nerror in filling image array";
			}

			SwsContext *swsCtx_;

			// Allocate and return swsContext.
			// a pointer to an allocated context, or NULL in case of error
			// Deprecated : Use sws_getCachedContext() instead.
			swsCtx_ = sws_getContext(pAVCodecContext->width, pAVCodecContext->height, pAVCodecContext->pix_fmt, outAVCodecContext->width, outAVCodecContext->height, outAVCodecContext->pix_fmt, SWS_BICUBIC, NULL, NULL, NULL);


			int ii = 0;
			int no_frames = 100;
			std::cout << "\nenter No. of frames to capture : ";
			std::cin >> no_frames;

			AVPacket outPacket;
			int j = 0;

			int got_picture;

			while (av_read_frame(pAVFormatContext, pAVPacket) >= 0) {
				if (ii++ == no_frames)
					break;
				if (pAVPacket->stream_index == VideoStreamIndx) {
					value = avcodec_decode_video2(pAVCodecContext, pAVFrame, &frameFinished, pAVPacket);
					if (value < 0) {
						std::cout << "unable to decode video";
					}

					if (frameFinished)// Frame successfully decoded :)
					{
						sws_scale(swsCtx_, pAVFrame->data, pAVFrame->linesize, 0, pAVCodecContext->height, outFrame->data, outFrame->linesize);
						av_init_packet(&outPacket);
						outPacket.data = NULL;    // packet data will be allocated by the encoder
						outPacket.size = 0;

						avcodec_encode_video2(outAVCodecContext, &outPacket, outFrame, &got_picture);

						if (got_picture) {
							if (outPacket.pts != AV_NOPTS_VALUE)
								outPacket.pts = av_rescale_q(outPacket.pts, video_st->codec->time_base, video_st->time_base);
							if (outPacket.dts != AV_NOPTS_VALUE)
								outPacket.dts = av_rescale_q(outPacket.dts, video_st->codec->time_base, video_st->time_base);

							printf("Write frame %3d (size= %2d)\n", j++, outPacket.size / 1000);
							if (av_write_frame(outAVFormatContext, &outPacket) != 0) {
								std::cout << "\nerror in writing video frame";
							}

							av_packet_unref(&outPacket);
						} // got_picture

						av_packet_unref(&outPacket);
					} // frameFinished

				}
			}// End of while-loop

			value = av_write_trailer(outAVFormatContext);
			if (value < 0) {
				std::cout << "\nerror in writing av trailer";
				exit(1);
			}


			//THIS WAS ADDED LATER
			av_free(video_outbuf);
			return 0;
		}

	};

	// TODO: check return values of all funcs.
}
