#pragma once

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

namespace NCV
{

	inline int Camera(int fps = 30, int xx = 1920, int yy = 1080)
	{
		auto fourcc = cv::VideoWriter::fourcc('M', 'J', 'P', 'G');
		cv::VideoCapture cap(0, cv::VideoCaptureAPIs::CAP_DSHOW);
		cap.set(cv::VideoCaptureProperties::CAP_PROP_FPS, fps);
		cap.set(cv::VideoCaptureProperties::CAP_PROP_FRAME_WIDTH, xx);
		cap.set(cv::VideoCaptureProperties::CAP_PROP_FRAME_HEIGHT, yy);
		cap.set(cv::VideoCaptureProperties::CAP_PROP_FOURCC, fourcc);

		if (!cap.isOpened()) {
			std::cout << "Failed to open camera" << std::endl;
			return 1;
		}

		double width = cap.get(cv::VideoCaptureProperties::CAP_PROP_FRAME_WIDTH);
		double height = cap.get(cv::VideoCaptureProperties::CAP_PROP_FRAME_HEIGHT);
		namedWindow("Test", cv::WindowFlags::WINDOW_AUTOSIZE);
		//namedWindow("Thresh", cv::WindowFlags::WINDOW_AUTOSIZE);
		cv::VideoWriter video("./out.avi", fourcc, fps, cv::Size(width, height), true);

		bool readCamera = false;
		while (!readCamera) {
			cv::Mat frame;
			bool success = cap.read(frame);
			if (!success) {
				std::cout << "Failed to read frame" << std::endl;
				break;
			}
			/*
			cv::Mat detectedFrame = frame.clone();
			cv::Mat thresh = frame.clone();
			cvtColor(thresh, thresh, cv::ColorConversionCodes::COLOR_BGR2GRAY);
			GaussianBlur(thresh, thresh, Size(1, 1), 100);
			threshold(thresh, thresh, threshholdValue, 255, cv::ThresholdTypes::THRESH_BINARY);

			std::vector<std::vector<cv::Point> > contours;
			std::vector hierarchy;
			findContours(thresh, contours, hierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE, Point(0, 0));
			cv::Mat contourOutput = image.clone();
			cv::findContours( contourOutput, contours, CV_RETR_LIST, CV_CHAIN_APPROX_NONE );

			std::vector > approx(contours.size());
			std::vector boundRects(contours.size());

			for (size_t cIndex = 0; cIndex < contours.size(); ++cIndex)
			{
				std::vector contour = contours[cIndex];
				double perimeter = arcLength(contour, true);

				if (perimeter > 500.0 && hierarchy[cIndex][3] == -1){

					approxPolyDP(cv::Mat(contour), approx[cIndex], 0.001 * perimeter, true);
					boundRects[cIndex] = boundingRect(cv::Mat(approx[cIndex]));

					rectangle(detectedFrame, boundRects[cIndex].tl(), boundRects[cIndex].br(), rectColor, 4, 8, 0);
				}
			}
			imshow("Original", detectedFrame);
			imshow("Thresh", thresh);
			video.write(detectedFrame);
			*/

			imshow("Test", frame);
			video.write(frame);

			int key = cv::waitKey(1000.0/fps);
			switch (key) {
				case 27:
					readCamera = true;
					break;
			}
		}
		cv::destroyWindow("Test");
		return 0;
	}
}