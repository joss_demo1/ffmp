#pragma once

#include <iostream>

extern "C" {
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <libavutil/imgutils.h>
#include <libavutil/opt.h>
}

namespace NEncode
{
    inline int Test(std::string ouf = "./out.mp4", int rx = 1920, int ry = 1080, int fps = 24)
    {
        auto filename = ouf.c_str();
        const AVOutputFormat *fmt;
        AVFormatContext *fctx = nullptr;
        AVCodecContext *cctx = nullptr;
        AVStream *st;

        avformat_alloc_output_context2(&fctx, nullptr, nullptr, filename);
        if (!fctx) {
            std::cout << "Error avformat_alloc_output_context2()" << std::endl;
            return -1;
        }

        fmt = fctx->oformat;

        st = avformat_new_stream(fctx, nullptr);
        if (!st) {
            std::cout << "Error avformat_new_stream()" << std::endl;
            avformat_free_context(fctx);
            return -1;
        }

        st->codecpar->codec_id = fmt->video_codec;
        st->codecpar->codec_type = AVMEDIA_TYPE_VIDEO;
        st->codecpar->width = rx;
        st->codecpar->height = ry;
        st->time_base = {1, fps};

        const AVCodec *pCodec = avcodec_find_encoder(st->codecpar->codec_id);
        if (!pCodec) {
            std::cout << "Error avcodec_find_encoder()" << std::endl;
            avformat_free_context(fctx);
            return -1;
        }

        cctx = avcodec_alloc_context3(pCodec);
        if (!cctx) {
            std::cout << "Error avcodec_alloc_context3()" << std::endl;
            avformat_free_context(fctx);
            return -1;
        }

        avcodec_parameters_to_context(cctx, st->codecpar);
        cctx->bit_rate = 400000;
        cctx->width = rx;
        cctx->height = ry;
        cctx->time_base = {1, fps};
        cctx->gop_size = 12;
        cctx->pix_fmt = AV_PIX_FMT_YUV420P;
        if (st->codecpar->codec_id == AV_CODEC_ID_H264)
            av_opt_set(cctx->priv_data, "preset", "ultrafast", 0);
        if (fctx->oformat->flags & AVFMT_GLOBALHEADER)
            cctx->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;
        avcodec_parameters_from_context(st->codecpar, cctx);
        av_dump_format(fctx, 0, filename, 1);

        if (avcodec_open2(cctx, pCodec, nullptr) < 0) {
            std::cout << "Error avcodec_open2()" << std::endl;
            avcodec_free_context(&cctx);
            avformat_free_context(fctx);
            return -1;
        }
        if (!(fmt->flags & AVFMT_NOFILE)) {
            if (avio_open(&fctx->pb, filename, AVIO_FLAG_WRITE) < 0) {
                std::cout << "Error avio_open()" << std::endl;
                avcodec_free_context(&cctx);
                avformat_free_context(fctx);
                return -1;
            }
        }
        if (avformat_write_header(fctx, nullptr) < 0) {
            std::cout << "Error avformat_write_header()" << std::endl;
            avcodec_free_context(&cctx);
            avformat_free_context(fctx);
            return -1;
        }

        AVFrame *frame = av_frame_alloc();
        if (!frame) {
            std::cout << "Error av_frame_alloc()" << std::endl;
            avcodec_free_context(&cctx);
            avformat_free_context(fctx);
            return -1;
        }
        frame->format = cctx->pix_fmt;
        frame->width = cctx->width;
        frame->height = cctx->height;
        if (av_image_alloc(frame->data, frame->linesize, cctx->width, cctx->height, cctx->pix_fmt, 32) < 0) {
            std::cout << "Error av_image_alloc()" << std::endl;
            av_frame_free(&frame);
            avcodec_free_context(&cctx);
            avformat_free_context(fctx);
            return -1;
        }

        AVPacket *pkt = av_packet_alloc();
        if (!pkt) {
            std::cout << "Error av_packet_alloc()" << std::endl;
            av_freep(&frame->data[0]);
            av_frame_free(&frame);
            avcodec_free_context(&cctx);
            avformat_free_context(fctx);
            return -1;
        }

        double video_pts = 0;
        for (int i = 0; i < 250; i++) {
            video_pts = static_cast<double>(cctx->time_base.num) / cctx->time_base.den * 90 * i;

            for (int y = 0; y < cctx->height; y++) {
                for (int x = 0; x < cctx->width; x++) {
                    frame->data[0][y * frame->linesize[0] + x] = x + y + i * 3;
                    if (y < cctx->height / 2 && x < cctx->width / 2) {
                        frame->data[1][y * frame->linesize[1] + x] = 128 + y + i * 2;
                        frame->data[2][y * frame->linesize[2] + x] = 64 + x + i * 5;
                    }
                }
            }

            av_init_packet(pkt);
            pkt->flags |= AV_PKT_FLAG_KEY;
            pkt->pts = frame->pts = video_pts;
            pkt->data = nullptr;
            pkt->size = 0;
            pkt->stream_index = st->index;

            if (avcodec_send_frame(cctx, frame) < 0) {
                std::cout << "Error avcodec_send_frame()" << std::endl;
                break;
            }
            while (avcodec_receive_packet(cctx, pkt) == 0) {
                av_interleaved_write_frame(fctx, pkt);
                av_packet_unref(pkt);
            }
        }

        for (;;) {
            avcodec_send_frame(cctx, nullptr);
            if (avcodec_receive_packet(cctx, pkt) == 0) {
                av_interleaved_write_frame(fctx, pkt);
                av_packet_unref(pkt);
            } else {
                break;
            }
        }

        av_write_trailer(fctx);
        if (!(fmt->flags & AVFMT_NOFILE)) {
            avio_closep(&fctx->pb);
        }
        av_freep(&frame->data[0]);
        av_frame_free(&frame);
        av_packet_free(&pkt);
        avcodec_free_context(&cctx);
        avformat_free_context(fctx);
        return 0;
    }
}
