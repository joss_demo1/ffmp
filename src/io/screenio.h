#pragma once

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <cstring>
#include <cmath>
#include <string>

#define __STDC_CONSTANT_MACROS

//FFMPEG LIBRARIES
extern "C" {
#include "libavcodec/avcodec.h"
#include "libavcodec/avfft.h"
#include "libavdevice/avdevice.h"
#include "libavfilter/avfilter.h"
#include "libavfilter/buffersink.h"
#include "libavfilter/buffersrc.h"
#include "libavformat/avformat.h"
#include "libavformat/avio.h"

// libav resample
#include "libavutil/opt.h"
#include "libavutil/common.h"
#include "libavutil/channel_layout.h"
#include "libavutil/imgutils.h"
#include "libavutil/mathematics.h"
#include "libavutil/samplefmt.h"
#include "libavutil/time.h"
#include "libavutil/pixdesc.h"
#include "libavutil/file.h"
// lib swresample
#include "libswscale/swscale.h"
}

namespace NScreen {

    class ScreenRecorder {
    private:
        const AVInputFormat* pAVInputFormat;
        const AVOutputFormat* output_format;

        AVCodecContext* pAVCodecContext;
        AVCodecContext* outAVCodecContext;

        AVFormatContext* pAVFormatContext;
        AVFormatContext* outAVFormatContext;

        AVFrame* pAVFrame;
        AVFrame* outFrame;

        const AVCodec* pAVCodec;
        const AVCodec* outAVCodec;

        AVPacket* pAVPacket;

        AVDictionary* options;

        AVStream* video_st;
        AVFrame* outAVFrame;

        const char* dev_name;
        const char* output_file;

        double video_pts;

        int out_size;
        int codec_id;
        int value;
        int VideoStreamIndx;

    public:

        inline ScreenRecorder() {
            /* initialize the resources */
            avdevice_register_all();
            std::cout << "\nAll required functions are registered successfully";
        }

        ~ScreenRecorder() {
            /* uninitialize the resources */
            avformat_close_input(&pAVFormatContext);
            if (!pAVFormatContext) {
                std::cout << "\nFile closed successfully";
            } else {
                std::cout << "\nUnable to close the file";
                exit(1);
            }

            avformat_free_context(pAVFormatContext);
            if (!pAVFormatContext) {
                std::cout << "\navformat free successfully";
            } else {
                std::cout << "\nUnable to free avformat context";
                exit(1);
            }
        }

        int openCamera() {
            /* function to initiate communication with display library */
            /* establishing the connection between camera or screen through its respective folder */
            value = 0;
            options = nullptr;
            pAVFormatContext = nullptr;

            pAVFormatContext = avformat_alloc_context(); // Allocate an AVFormatContext.

            pAVInputFormat = av_find_input_format("x11grab");
            value = avformat_open_input(&pAVFormatContext, ":0.0+10,250", pAVInputFormat, nullptr);
            if (value != 0) {
                std::cout << "\nError in opening input device";
                exit(1);
            }

            /* set frame per second */
            value = av_dict_set(&options, "framerate", "30", 0);
            if (value < 0) {
                std::cout << "\nError in setting dictionary value";
                exit(1);
            }

            value = av_dict_set(&options, "preset", "medium", 0);
            if (value < 0) {
                std::cout << "\nError in setting preset values";
                exit(1);
            }

            value = avformat_find_stream_info(pAVFormatContext, nullptr);
            if (value < 0) {
                std::cout << "\nUnable to find the stream information";
                exit(1);
            }

            VideoStreamIndx = -1;

            /* find the first video stream index */
            for (int i = 0; i < pAVFormatContext->nb_streams; i++) // find video stream position/index.
            {
                if (pAVFormatContext->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_VIDEO) {
                    VideoStreamIndx = i;
                    break;
                }
            }

            if (VideoStreamIndx == -1) {
                std::cout << "\nUnable to find the video stream index. (-1)";
                exit(1);
            }

            pAVCodecContext = avcodec_alloc_context3(nullptr);
            avcodec_parameters_to_context(pAVCodecContext, pAVFormatContext->streams[VideoStreamIndx]->codecpar);

            pAVCodec = avcodec_find_decoder(pAVCodecContext->codec_id);
            if (pAVCodec == nullptr) {
                std::cout << "\nUnable to find the decoder";
                exit(1);
            }

            value = avcodec_open2(pAVCodecContext, pAVCodec, nullptr); // Initialize the AVCodecContext to use the given AVCodec.
            if (value < 0) {
                std::cout << "\nUnable to open the av codec";
                exit(1);
            }
            return 0;
        }

        int init_outputfile() {
            /* initialize the video output file and its properties */
            outAVFormatContext = nullptr;
            value = 0;
            output_file = "../media/output.mp4";

            avformat_alloc_output_context2(&outAVFormatContext, nullptr, nullptr, output_file);
            if (!outAVFormatContext) {
                std::cout << "\nError in allocating av format output context";
                exit(1);
            }

            output_format = av_guess_format(nullptr, output_file, nullptr);
            if (!output_format) {
                std::cout << "\nError in guessing the video format. Try with correct format";
                exit(1);
            }

            video_st = avformat_new_stream(outAVFormatContext, nullptr);
            if (!video_st) {
                std::cout << "\nError in creating an av format new stream";
                exit(1);
            }

            outAVCodecContext = avcodec_alloc_context3(nullptr);
            if (!outAVCodecContext) {
                std::cout << "\nError in allocating the codec contexts";
                exit(1);
            }

            outAVCodec = avcodec_find_encoder(AV_CODEC_ID_MPEG4);
            if (!outAVCodec) {
                std::cout << "\nError in finding the av codecs. Try again with correct codec";
                exit(1);
            }

            outAVCodecContext->codec_id = AV_CODEC_ID_MPEG4;
            outAVCodecContext->codec_type = AVMEDIA_TYPE_VIDEO;
            outAVCodecContext->pix_fmt = AV_PIX_FMT_YUV420P;
            outAVCodecContext->bit_rate = 400000;
            outAVCodecContext->width = 1920;
            outAVCodecContext->height = 1080;
            outAVCodecContext->gop_size = 3;
            outAVCodecContext->max_b_frames = 2;
            outAVCodecContext->time_base = { 1, 30 };

            if (codec_id == AV_CODEC_ID_H264) {
                av_opt_set(outAVCodecContext->priv_data, "preset", "slow", 0);
            }

            avcodec_parameters_from_context(video_st->codecpar, outAVCodecContext);

            if (outAVFormatContext->oformat->flags & AVFMT_GLOBALHEADER) {
                outAVCodecContext->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;
            }

            value = avcodec_open2(outAVCodecContext, outAVCodec, nullptr);
            if (value < 0) {
                std::cout << "\nError in opening the avcodec";
                exit(1);
            }

            /* create empty video file */
            if (!(outAVFormatContext->flags & AVFMT_NOFILE)) {
                if (avio_open2(&outAVFormatContext->pb, output_file, AVIO_FLAG_WRITE, nullptr, nullptr) < 0) {
                    std::cout << "\nError in creating the video file";
                    exit(1);
                }
            }

            if (!outAVFormatContext->nb_streams) {
                std::cout << "\nOutput file does not contain any stream";
                exit(1);
            }

            value = avformat_write_header(outAVFormatContext, &options);
            if (value < 0) {
                std::cout << "\nError in writing the header context";
                exit(1);
            }

            return 0;
        }

        int CaptureVideoFrames() {
            /* function to capture and store data in frames by allocating required memory and auto deallocating the memory */
            int frameFinished;

            int frame_index = 0;
            value = 0;

            pAVPacket = av_packet_alloc();
            if (!pAVPacket) {
                std::cout << "\nError allocating AVPacket";
                exit(1);
            }

            pAVFrame = av_frame_alloc();
            if (!pAVFrame) {
                std::cout << "\nUnable to allocate AVFrame";
                exit(1);
            }

            outFrame = av_frame_alloc();
            if (!outFrame) {
                std::cout << "\nUnable to allocate outFrame";
                exit(1);
            }

            int nbytes = av_image_get_buffer_size(outAVCodecContext->pix_fmt, outAVCodecContext->width, outAVCodecContext->height, 32);
            uint8_t* video_outbuf = (uint8_t*)av_malloc(nbytes);
            if (video_outbuf == nullptr) {
                std::cout << "\nUnable to allocate memory";
                exit(1);
            }

            value = av_image_fill_arrays(outFrame->data, outFrame->linesize, video_outbuf, outAVCodecContext->pix_fmt, outAVCodecContext->width, outAVCodecContext->height, 1);
            if (value < 0) {
                std::cout << "\nError in filling image array";
            }

            SwsContext* swsCtx_ = sws_getContext(pAVCodecContext->width, pAVCodecContext->height, pAVCodecContext->pix_fmt,
                                                 outAVCodecContext->width, outAVCodecContext->height, outAVCodecContext->pix_fmt, SWS_BICUBIC, nullptr, nullptr, nullptr);

            int ii = 0;
            int no_frames = 100;
            std::cout << "\nEnter No. of frames to capture: ";
            std::cin >> no_frames;

            AVPacket outPacket;
            int j = 0;
            int got_picture;

            while (av_read_frame(pAVFormatContext, pAVPacket) >= 0) {
                if (ii++ == no_frames)
                    break;
                if (pAVPacket->stream_index == VideoStreamIndx) {
                    if (avcodec_send_packet(pAVCodecContext, pAVPacket) < 0) {
                        std::cout << "Error sending a packet for decoding";
                        continue;
                    }

                    while (avcodec_receive_frame(pAVCodecContext, pAVFrame) == 0) {
                        sws_scale(swsCtx_, pAVFrame->data, pAVFrame->linesize, 0, pAVCodecContext->height, outFrame->data, outFrame->linesize);
                        av_init_packet(&outPacket);
                        outPacket.data = nullptr;
                        outPacket.size = 0;

                        if (avcodec_send_frame(outAVCodecContext, outFrame) < 0) {
                            std::cout << "Error sending a frame for encoding";
                            break;
                        }

                        while (avcodec_receive_packet(outAVCodecContext, &outPacket) == 0) {
                            if (outPacket.pts != AV_NOPTS_VALUE)
                                outPacket.pts = av_rescale_q(outPacket.pts, outAVCodecContext->time_base, video_st->time_base);
                            if (outPacket.dts != AV_NOPTS_VALUE)
                                outPacket.dts = av_rescale_q(outPacket.dts, outAVCodecContext->time_base, video_st->time_base);

                            std::cout << "Write frame " << j++ << " (size= " << outPacket.size / 1000 << ")\n";
                            if (av_write_frame(outAVFormatContext, &outPacket) != 0) {
                                std::cout << "\nError in writing video frame";
                            }

                            av_packet_unref(&outPacket);
                        }
                    }
                }
                av_packet_unref(pAVPacket);
            }

            value = av_write_trailer(outAVFormatContext);
            if (value < 0) {
                std::cout << "\nError in writing av trailer";
                exit(1);
            }

            av_free(video_outbuf);
            return 0;
        }
    };
}
