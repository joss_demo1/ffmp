#pragma once

#include "headers.h"

/*
by default class if not doing anything automatically:
i.e. constructor called with filename just sets the class member, but not reading the file

Workflow:
FileIO f(fname);
f.read();
string = f.getData();
*/
namespace NFile
{
	typedef std::vector<unsigned char> byteArray;

	class FileIO
	{
		/*!
		*
		* \class FileIO
		* \brief Simple file handling class
		*
		* \author joss1
		* \date December 2022
		*/
	public:
		inline FileIO(std::string &fname)
		{
			clear();
			set(fname);
		}

		~FileIO()
		{
		}

		void clear()
		{
			_buffer.clear();
			_hasData = false;
			_fname = "";
			_fbak = "";
		}


		void set(std::string &fname)
		{
			_fname = fname;
			_hasData = false;
		}

		void setData(byteArray &indata)
		{
			_buffer = indata;
			_hasData = true;
		}

		void setDataS(std::string &indata)
		{
			_buffer.clear();
			_buffer = byteArray{indata.begin(), indata.end()};
			_hasData = true;
		}

		std::string get(void) const
		{
			return _fname;
		}

		std::string getBak(void) const
		{
			return _fbak;
		}

		byteArray getData(void) const
		{
			return _buffer;
		}

		std::string getDataS(void) const
		{
			std::string strbuf{_buffer.begin(), _buffer.end()};
			return strbuf;
		}


		void read(void)
		{
			if (!_fname.size())
				return;
			_buffer = _readFile(_fname);
			_hasData = true;
		}

		int write(void)
		{
			if (_hasData)
				return _writeFile(_fname, _buffer);
			return 0;
		}

		_int64 length(void)
		{
			return static_cast<_int64>(_buffer.size());
		}


		bool backup()
		{
			namespace fs = std::filesystem;
			const fs::path srcPath = _fname;
			const fs::path dstPath = _fname + ".bak";
			fs::copy_options cpOpts = fs::copy_options::overwrite_existing;
			std::error_code err;

			auto result = fs::copy_file(srcPath, dstPath, cpOpts, err);
			if (result)
				_fbak = dstPath.generic_string();
			else
				_fbak = std::string("");
			return result;
		}


	private:
		std::string _fname;
		std::string _fbak;
		int _hasData;
		byteArray _buffer;

		// private:
		inline byteArray _readFile(std::string &fname)
		{
			std::ifstream F;
			F.open(fname, std::ios::binary | std::ios::ate);
			if (!F) {
				return byteArray();
			}
			_int64 flen = static_cast<_int64>(F.tellg());
			if (flen <= 0) {
				F.close();
				return byteArray();
			}
			F.seekg(0, std::ios_base::beg);

			byteArray buf(flen);
			F.read(reinterpret_cast<char *>(buf.data()), flen);
			return buf;
		}

		inline bool _writeFile(std::string &fname, byteArray &buffer)
		{
			std::ofstream F;
			F.open(fname, std::ios::out | std::ios::binary);
			if (!F)
				return false;
			F.write(reinterpret_cast<char *>(buffer.data()), buffer.size());
			return true;
		}
	};
}