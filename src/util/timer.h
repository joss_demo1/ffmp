#pragma once

#include "headers.h"
#include <windows.h>


// idea taken from: cplus.about.com/od/howtodothingsin1/a/timing.htm and reimplemented as a class


struct Time
{
	LARGE_INTEGER start;
	LARGE_INTEGER stop;
};


class Timer
{
public:
	inline Timer(std::string name = std::string("Default_timer"))
	{
		_name = name;
		_time.start.QuadPart = 0;
		_time.stop.QuadPart = 0;
		QueryPerformanceFrequency(&frequency);
	}

	inline ~Timer()
	{}

	inline void start()
	{
		QueryPerformanceCounter(&_time.start);
	}

	inline void stop(String &msg)
	{
		QueryPerformanceCounter(&_time.stop);
		auto t = getElapsedTime();
		if (msg.size())
			//std::cout << _name << ": " << msg << " in " << t << "s" << std::endl;
			std::cout << fmt::format("{}: {} in {:.8f}s\n", _name, msg, t);
		else
			//std::cout << _name << " is done in: " << t << "s" << std::endl;
			std::cout << fmt::format("{} is done in: {:.8f}s\n", _name, t);
	}


	double getElapsedTime()
	{
		LARGE_INTEGER time;
		time.QuadPart = _time.stop.QuadPart - _time.start.QuadPart;
		return LIToSecs(time);
	}

	double LIToSecs(LARGE_INTEGER &L)
	{ return ((double) L.QuadPart / (double) frequency.QuadPart); }

private:
	std::string _name;
	Time _time;
	LARGE_INTEGER frequency;
};
