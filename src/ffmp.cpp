#include "headers.h"

#include "Python.h"

#if PY_MAJOR_VERSION >= 3
#define PyStr_Check PyBytes_Check
#define PyStr_Size PyBytes_Size
#define PyStr_AsString PyBytes_AsString
#else
#define PyStr_Check PyString_Check
#define PyStr_Size PyString_GET_SIZE
#define PyStr_AsString PyString_AS_STRING
#endif


#include "io/fileio.h"
#include "io/ffmpegio.h"
#include "io/imageio.h"
#include "io/opencvio.h"
#include "io/screenio.h"

#include "image/tchannel.h"
#include "image/timage.h"
#include "image/tstack.h"

#include "util/timer.h"

#include "mainwin/mainwin.h"


/*
	qimage_p2c(PyObject* qimg)	// Convert Python QImage to C++ QImage using .bytes()
 	encode_video(std::vector<QImage>, std::string ouf);
 	std::vector<QImage> decode_video(std::string inf);

	void encode_test(ouf='./temp.mp4', x=1920, y=1080, fps=24)
/* */


template<typename T> std::vector<T> pylist2strings(PyObject *py_list, T (*ConvertToT)(PyObject *))
{
	assert(cpython_asserts(py_list));
	std::vector<T> cpp_vector;

	if (PyList_Check(py_list)) {
		cpp_vector.reserve(PyList_GET_SIZE(py_list));
		for (Py_ssize_t i = 0; i < PyList_GET_SIZE(py_list); ++i) {
			cpp_vector.emplace(cpp_vector.end(), (*ConvertToT)(PyList_GetItem(py_list, i)));
			if (PyErr_Occurred()) {
				cpp_vector.clear();
				break;
			}
		}
	} else {
		PyErr_Format(PyExc_TypeError, "Argument \"py_list\" to %s must be list not \"%s\"", __FUNCTION__, Py_TYPE(py_list)->tp_name);
	}
	return cpp_vector;
}


Strings get_str_list(PyObject *args)
{
	// Get Python List of strings
	Strings lst;
	PyObject *pList;
	if (!PyArg_ParseTuple(args, "O!", &PyList_Type, &pList)) {
		PyErr_SetString(PyExc_TypeError, "arg must be a list.");
		return lst;
	}

	PyObject *pItem;
	Py_ssize_t n;
	int i;
	n = PyList_Size(pList);
	for (i = 0; i < n; i++) {
		pItem = PyList_GetItem(pList, i);
		/*if (!PyStr_Check(pItem)) {
			PyErr_SetString(PyExc_TypeError, "list items must be integers.");
			return lst;
		}*/
		auto item = PyStr_AsString(pItem);
		lst.push_back(String(item));
	}
	return lst;
}


String get_str(PyObject *args)
{
	// Get Python String
	String res;
	char *in_str;
	if (!PyArg_ParseTuple(args, "s*", &in_str)) {
		return res;
	}
	res = in_str;
	return res;
}


int test_qt(int argc, char *argv[])
{
	// Qt test app
	QApplication App(argc, argv);
	//QMainWindow Win;
	MainWin Win;
	Win.show();
	App.exec();
	return 0;
}


static PyObject *add(PyObject *self, PyObject *args)
{
	int x = 0, y = 0, s = 0;
	PyArg_ParseTuple(args, "ii", &x, &y);
	s = x + y;
	PyObject *python_val = Py_BuildValue("i", s);
	return python_val;
}


static PyObject *encode(PyObject *self, PyObject *args)
{
	try {
		NEncode::Test();
	} catch (...) {
		std::cout << "Exception!" << std::endl;
	}
	srand(time(NULL));    // initialize random seed
	int random = rand() % 10;
	PyObject *python_val = Py_BuildValue("i", random);
	return python_val;
}


static PyObject *cam(PyObject *self, PyObject *args)
{
	try {
		//opencv_encode(30, 3840, 2160);
		NCV::Camera(30, 1920, 1080);
	} catch (...) {
		std::cout << "Exception!" << std::endl;
	}
	PyObject *python_val = Py_BuildValue("i", 0);
	return python_val;
}


static PyObject *screen(PyObject *self, PyObject *args)
{
	try {
		NScreen::ScreenRecorder scr;
		scr.openCamera();
		scr.init_outputfile();
		scr.CaptureVideoFrames();
	} catch (...) {
		std::cout << "Exception!" << std::endl;
	}
	PyObject *python_val = Py_BuildValue("i", 0);
	return python_val;
}


static PyObject *qt(PyObject *self, PyObject *args)
{
	int i = 1;
	auto s = std::string("test");
	char *lst[1];
	lst[0] = (char *) s.c_str();;
	auto res = test_qt(i, lst);
	PyObject *python_val = Py_BuildValue("s", res);
	return python_val;
}


static PyObject *vdb(PyObject *self, PyObject *args)
{
	openvdb::initialize();
	std::cout << String(fmt::format("vdb():\n"));
	auto lst = get_str_list(args);
	std::cout << String(fmt::format("vdb(): parsed str list\n"));
	std::cout << fmt::format("VDB(): processing {} files\n", lst.size());
	auto cntr = 0;
	auto sz = lst.size();
	Stack istack;
	std::cout << String(fmt::format("vdb(): enter loop\n"));
	for (auto &f: lst) {
		Timer timer(fmt::format("Processing [{}/{}]: '{}'", cntr, sz, f));
		auto img = Image(f);
		auto msg = String(fmt::format("file '{}' processed", f));
		timer.stop(msg);
		cntr++;
		istack += img;
	}

	auto mnx = istack.minmax();
	auto dmin = mnx[0];
	auto dmax = mnx[1];
	auto mx = istack.width();
	auto my = istack.height();
	auto mz = istack.depth();

	String ouf("./out.vdb");
	std::cout << fmt::format("Writing image stack {} (minmax {}-{}) to VDB file: {}\n", istack.depth(), dmin, dmax, ouf);
	istack.write_vdb(ouf);

	auto ouf_str = ouf.c_str();
	PyObject *python_val = Py_BuildValue("s", ouf_str);
	return python_val;
}


static PyObject *img(PyObject *self, PyObject *args)
{
	auto fpn = get_str(args);
	//std::replace(fpn.begin(), fpn.end(), '\\', '/');
	auto img = Image(fpn);
	std::stringstream sstr;
	sstr << img;
	//auto msg = String(fmt::format("load_img(): done: {}\n", sstr.str()));
	PyObject *python_val = Py_BuildValue("s", sstr.str().c_str());
	return python_val;
}


/*
 * module name
 * module docs
 * size of per-interpreter state of the module, or -1 if the module keeps state in global variables.
 * functions struct
 */
static PyMethodDef module_methods[] = {{"add",    add,    METH_VARARGS, "Basic method test"},
									   {"encode", encode, METH_NOARGS,  "Camera recorder"},
									   {"cam",    cam,    METH_NOARGS,  "Camera recorder-2"},
									   {"screen", screen, METH_VARARGS, "Screen recorder"},
									   {"qt",     qt,     METH_VARARGS, "Qt test"},
									   {"vdb",    vdb,    METH_VARARGS, "Stack images into a VDB file"},
									   {"img",    img,    METH_VARARGS, "Load an image file"},
									   {NULL, NULL, 0, NULL}};
static struct PyModuleDef FFMP = {PyModuleDef_HEAD_INIT, "ffmp", "", -1, module_methods};

PyMODINIT_FUNC PyInit_ffmp(void)
{
	return PyModule_Create(&FFMP);
}
