#pragma once
#include "headers.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWin; }
QT_END_NAMESPACE

inline std::string que() {
	int i = 1;
	auto s = std::string("test");
	auto a = (char*)s.c_str();
	char *lst[1];
	lst[0] = a;
	QApplication App(i, lst);
	auto pth = QCoreApplication::applicationDirPath();
	auto pths = pth.toStdString();
	std::cout << pths;
	return pths;
}

class MainWin : public QMainWindow
{
    Q_OBJECT

public:
    MainWin(QWidget *parent = nullptr);
    ~MainWin();

private:
    Ui::MainWin *ui;
};
