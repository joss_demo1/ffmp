#include "mainwin.h"
#include "./ui_mainwin.h"

// https://doc.qt.io/qt-5/designer-using-a-ui-file.html

MainWin::MainWin(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWin)
{
	ui->setupUi(this);
}

MainWin::~MainWin()
{
	delete ui;
}
