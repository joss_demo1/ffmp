#include "tchannel.h"

#pragma once

class TImage
{
	/*!
	 * \class TImage
	 *
	 * \brief A collection of Channels
	 *
	 * \author joss1
	 * \date October 2019
	 */
	typedef std::vector<Channel> Channels;

private:
	Channels data;

public:
	TImage()
	{ clear(); }

	TImage(const TImage &b)
	{ *this = b; }

	//
	TImage(String &fname)
	{ *this = TImage::from_file(fname); }

	TImage(uint32_t width, uint32_t height, uint8_t chans, Bytes &buffer)
	{ *this = TImage::from_mem_flat(width, height, chans, buffer); }

	TImage(uint32_t width, uint32_t height, uint8_t chans, Bytes &buffer, uint8_t rgba)
	{ *this = TImage::from_mem_rgba(width, height, chans, buffer); }

	//
	void clear()
	{ data.clear(); }

	auto size()
	{ return data.size(); }

	auto width()
	{
		uint32_t res = 0;
		if (size())
			res = data[0].width;
		return res;
	}

	auto height()
	{
		uint32_t res = 0;
		if (size())
			res = data[0].height;
		return res;
	}

	auto xyz(uint32_t x, uint32_t y, uint32_t z)
	{
		if (z > size())
			z = 0;
		auto res = data.at(z).xy(x, y);
		return res;
	}

	auto minmax()
	{
		std::vector<Real> res;
		Real mn = 65535;
		Real mx = 0;
		uint32_t sz = width() * height();
		for (auto &ch: data) {
			auto mnmx = ch.minmax();
			auto mnc = mnmx[0];
			auto mxc = mnmx[1];
			if (mx < mxc)
				mx = mxc;
			if (mn > mnc)
				mn = mnc;
		}
		res.push_back(mn);
		res.push_back(mx);
		return res;
	}


	void append(const Channel &chan)
	{
		data.push_back(chan);
	}


	static TImage from_mem_flat(uint32_t width, uint32_t height, uint8_t chans, Bytes &buffer)
	{
		/*
		[channel][channel][channel] pack
		*/
		TImage res;
		assert(width * height * chans == buffer.size());
		uint32_t inc = width * height;
		uint32_t ptr = 0;
		for (auto i = 0u; i < chans; i++) {
			//
			assert(ptr < buffer.size());
			assert(ptr + inc <= buffer.size());
			//
			auto nm = std::to_string(i);
			auto start = buffer.begin() + ptr;
			auto end = buffer.begin() + ptr + inc;
			auto chan = Channel(nm, width, height, start, end);
			res.append(chan);
			ptr += inc;
		}
		return res;
	}


	static TImage from_mem_rgba(uint32_t width, uint32_t height, uint8_t chans, Bytes &buffer)
	{
		/*
		[rgba][rgba][rgba]
		*/
		TImage res;
		assert(width * height * chans == buffer.size());
		auto sz = width * height;
		for (auto i = 0; i < chans; i++) {
			auto nm = std::to_string(i);
			auto ch = Channel(nm, width, height, buffer, chans, i);
			res.append(ch);
		}
		return res;
	}


	inline static TImage from_file(String &fname)
	{
		/*
			Init from image
		*/
		using namespace OIIO;
		//
		auto fname_str = fname.c_str();
		auto in = ImageInput::open(fname_str);
		if (!in) {
			std::cout << "OIIO: ERROR reading file\n";
			std::cout << geterror() << std::endl;
		}
		const ImageSpec &spec = in->spec();
		uint32_t xx = spec.width;
		uint32_t yy = spec.height;
		uint8_t chans = spec.nchannels;
		//std::vector<unsigned char> buffer(xx * yy * chans);
		Bytes buffer(xx * yy * chans);
		// #TODO: depth
		in->read_image(TypeDesc::UINT8, &buffer[0]);
		in->close();
		//
		auto res = TImage(xx, yy, chans, buffer, 0);
		//std::cout << fmt::format("{} -> {}", fname, res) << std::endl;
		return res;
	}


	// cout support
	friend std::ostream &operator<<(std::ostream &ostr, TImage &b)
	{
		auto w = b.width();
		auto res = fmt::format("TImage({}x{}, {})", b.width(), b.height(), b.size());
		OSTR_SET
		ostr << res << std::endl;
		return ostr;
	}


	TImage &operator=(const TImage &b)
	{
		if (this == &b)
			return (*this);
		clear();
		data = b.data;
		return (*this);
	}

	Channel operator[](uint32_t i) const
	{ return data[i]; }

	Channel &operator[](uint32_t i)
	{ return data.at(i); }
};

typedef TImage Image;
