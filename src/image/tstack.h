#pragma once

#include "timage.h"

class TStack
	/*
		Stack of Frames()
	*/
{
	typedef std::vector<Image> Images;
public:
	Images data;

	//==--- Service stuff start
	TStack()
	{ clear(); }

	TStack(const TStack &b)
	{ *this = b; }

	TStack(uint32_t num)
	{
		clear();
		data.resize(num);
	}

	TStack(Strings &flist)
	{ *this = TStack::from_filelist(flist); }

	//
	void clear()
	{ data.clear(); }

	auto size()
	{ return data.size(); }

	auto width()
	{
		uint32_t res = 0;
		if (data.size())
			res = data[0].width();
		return res;
	}

	auto height()
	{
		uint32_t res = 0;
		if (data.size())
			res = data[0].height();
		return res;
	}

	auto depth()
	{
		uint32_t res = size();
		return res;
	}

	void append(const Image &b)
	{ data.push_back(b); }

	void append(const TStack &b)
	{ data.insert(data.end(), b.data.begin(), b.data.end()); }


	// data accessors
	auto xyz(uint32_t x, uint32_t y, uint32_t z)
	{
		//uint16_t res = 0;
		//if (z > data.size()) return res;
		return data.at(z).xyz(x, y, 0);
	}

	auto minmax()
	{
		std::vector<uint16_t> mins;
		std::vector<uint16_t> maxs;
		for (auto &img: data) {
			auto tt = img.minmax();
			mins.push_back(tt[0]);
			maxs.push_back(tt[1]);
		}
		auto mn = *std::min_element(mins.begin(), mins.end());
		auto mx = *std::max_element(maxs.begin(), maxs.end());
		std::vector<uint16_t> res;
		res.push_back(mn);
		res.push_back(mx);
		return res;
	}


	// cout support
	friend std::ostream &operator<<(std::ostream &ostr, const TStack &b)
	{
		OSTR_SET
		ostr << "TStack<" << b.data.size() << " slices>" << std::endl;
		return ostr;
	}


	TStack &operator=(const TStack &b)
	{
		if (this == &b)
			return (*this);
		clear();
		data = b.data;
		return (*this);
	}


	//bool		operator==(const Stack& b) const { if (_data == b._data) return true; return false; }
	//bool		operator!=(const Stack& b) const { if (_data != b._data) return true; return false; }
	TStack &operator+=(const TStack &b)
	{
		this->append(b);
		return *this;
	}

	TStack operator+(const TStack &b)
	{
		TStack r(*this);
		r += b;
		return (r);
	}

	TStack &operator+=(const TImage &b)
	{
		this->append(b);
		return *this;
	}

	TStack operator+(const TImage &b)
	{
		TStack r(*this);
		r += b;
		return (r);
	}

	TImage operator[](Int i) const
	{ return data[i]; }

	TImage &operator[](Int i)
	{ return data.at(i); }


	//==--- Service stuff ends
	typedef openvdb::FloatGrid::Ptr FGridPtr;
	typedef openvdb::GridPtrVec FGridPtrList;
	//typedef std::vector<FGridPtr> FGridPtrList;
public:
	static FGridPtrList vdb_gen(TStack &stack, float tolerance = 0.01)
	{
		/*
			https://www.openvdb.org/documentation/doxygen/Dense_8h.html#details
		*/
		// bbox
		openvdb::Coord bmin(0, 0, 0);
		openvdb::Coord bmax(stack.width(), stack.height(), stack.depth());
		const openvdb::CoordBBox bbox(bmin, bmax);
		// grid
		openvdb::tools::Dense<float> dense_r(bbox), dense_g(bbox), dense_b(bbox);
		// densities
		auto mnmx = stack.minmax();
		float density_min = mnmx[0];
		float density_max = mnmx[1];
		std::cout << "density_max: " << density_max << std::endl;
		std::cout << "density_min: " << density_min << std::endl;
		// iter
		openvdb::Coord xyz;
		int &x = xyz[0], &y = xyz[1], &z = xyz[2];
		auto mx = bmax.x();
		auto my = bmax.y();
		auto mz = bmax.z();
		for (z = bmin.z(); z < mz; ++z) {
			auto msg = fmt::format("Processing image {:04d}: [x: {:04d}/{:04d}, y: {:04d}/{:04d}, z: {:04d}/{:04d}]\n", z, x, mx, y, my, z, mz);
			std::cout << msg << std::endl;
			for (y = bmin.y(); y < my; ++y) {
				for (x = bmin.x(); x < mx; ++x) {
					//auto cell = stack.xyz(x, y, z);
					auto cell_r = stack[z][0].xy(x, y);
					auto cell_g = stack[z][1].xy(x, y);
					auto cell_b = stack[z][2].xy(x, y);
					// #Important float is critical here!!!
					float dens_r = (float) cell_r / (density_max);
					float dens_g = (float) cell_g / (density_max);
					float dens_b = (float) cell_b / (density_max);
					dens_r = std::max(dens_r, 0.0f);
					dens_g = std::max(dens_g, 0.0f);
					dens_b = std::max(dens_b, 0.0f);
					dense_r.setValue(xyz, dens_r);
					dense_g.setValue(xyz, dens_g);
					dense_b.setValue(xyz, dens_b);
				}
			}
		}
		FGridPtrList grids;
		// R
		FGridPtr grid_r = openvdb::FloatGrid::create(0.0f);
		openvdb::tools::copyFromDense(dense_r, *grid_r, tolerance);
		grid_r->setName("r");
		grid_r->setGridClass(openvdb::GRID_FOG_VOLUME);
		grids.push_back(grid_r);
		// G
		FGridPtr grid_g = openvdb::FloatGrid::create(0.0f);
		openvdb::tools::copyFromDense(dense_g, *grid_g, tolerance);
		grid_g->setName("g");
		grid_g->setGridClass(openvdb::GRID_FOG_VOLUME);
		grids.push_back(grid_g);
		// B
		FGridPtr grid_b = openvdb::FloatGrid::create(0.0f);
		openvdb::tools::copyFromDense(dense_b, *grid_b, tolerance);
		grid_b->setName("b");
		grid_b->setGridClass(openvdb::GRID_FOG_VOLUME);
		grids.push_back(grid_b);
		return grids;
	}


	static int vdb_write(String &fname, FGridPtrList &grids)
	{
		openvdb::io::File file(fname.c_str());
		file.write(grids);
		file.close();
		return 1;
	}

	int write_vdb(String &fname)
	{
		String fpn = fname;
		if(!fpn.size()) {
			fpn = String("out.vdb");
		}
		auto grid = TStack::vdb_gen(*this);
		auto res = TStack::vdb_write(fpn, grid);
		std::cout << fmt::format("File {} has been written.\n", fpn);
		return 1;
	}


	static TStack from_filelist(Strings &flist)
	{
		/*
			Init from image
		*/
		auto num = flist.size();
		TStack res(num);
		// http://supercomputingblog.com/openmp/openmp-tutorial-the-basics/
		// https://pro-prof.com/archives/4335
		//#pragma omp parallel
		for (uint32_t i = 0; i < num; i++) {
			auto fname = flist[i];
			auto msg = fmt::format("Loading {}\n", fname);
			std::cout << msg << std::endl;
			res[i] = TImage::from_file(fname);
		}
		return res;
	}

	static TStack from_filelist_old(Strings &flist)
	{
		/*
			Init from image
		*/
		TStack res;
		for (auto &fname: flist) {
			auto msg = fmt::format("Loading {}\n", fname);
			std::cout << msg << std::endl;
			auto img = TImage::from_file(fname);
			res.append(img);
		}
		return res;
	}


};

typedef TStack Stack;



