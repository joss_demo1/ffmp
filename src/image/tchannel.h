#pragma once

#include "headers.h"

template<typename T> class TChannel
{
	/*!
	 * \class TChannel
	 *
	 * \brief Simple 2d channel representing 1 image channel.
	 *
	 * \author joss1
	 * \date October 2019
	 */
	typedef std::vector<T> Bytes;

public:
	String name;
	uint32_t width;
	uint32_t height;
	uint8_t depth;
	Bytes data;


	TChannel()
	{ clear(); }

	TChannel(const TChannel &b)
	{ *this = b; }

	TChannel(String &nm, uint32_t w, uint32_t h)
	{
		// empty channel buffer (allocated)
		clear();
		name = nm;
		width = w;
		height = h;
		auto sz = w * h;
		data = Bytes(sz, (T) 0);
		if (data.size())
			depth = sizeof(T);
		assert(width * height == data.size());
	}

	TChannel(String &nm, uint32_t w, uint32_t h, typename Bytes::iterator &start, typename Bytes::iterator &end)
	{
		// [channel] [channel] [channel] pack, just copying contiguous channels blocks
		// https://stackoverflow.com/questions/2931345/trouble-with-dependent-types-in-templates
		clear();
		name = nm;
		width = w;
		height = h;
		data = Bytes(start, end);
		if (data.size())
			depth = sizeof(T);
		assert(width * height == data.size());
	}

	TChannel(String &nm, uint32_t w, uint32_t h, Bytes &buffer, uint8_t numChannels, uint8_t num)
	{
		/*
		[rgba][rgba][rgba] pack
		buffer of rgba pixels: we need to know total number of channels and what channel we are.
		*/
		clear();
		name = nm;
		width = w;
		height = h;
		auto sz = width * height;
		data.resize(sz);
		for (uint32_t i = 0u; i < sz; i++) {
			auto buffer_ofs = i * numChannels + num;
			data[i] = buffer[buffer_ofs];
		}
		if (data.size())
			depth = sizeof(data[0]);
		assert(width * height == data.size());
	}

	void clear()
	{
		name = String("default");
		width = 0u;
		height = 0u;
		depth = 0u;
		data.clear();
	}

	auto size()
	{ return (data.size()); }


	auto offset(uint32_t x, uint32_t y)
	{
		uint32_t res = 0;
		if (y < height && x < width)
			res = y * width + x;
		return res;
	}

	auto xy(uint32_t x, uint32_t y)
	{
		auto ofs = offset(x, y);
		auto res = data.at(ofs);
		return res;
	}

	auto minmax()
	{
		std::vector<Real> res;
		Real mn = 65535;
		Real mx = 0;
		uint32_t sz = width * height;
		for (auto i = 0u; i < sz; i++) {
			auto vv = data[i];
			if (mx < vv)
				mx = vv;
			if (mn > vv)
				mn = vv;
		}
		res.push_back(mn);
		res.push_back(mx);
		return res;
	}

	// cout support
	friend std::ostream &operator<<(std::ostream &ostr, const TChannel &b)
	{
		auto res = fmt::format("TChannel({}x{}*{}, {})", width, height, depth, data.size());
		ostr << res << std::endl;
		return ostr;
	}


	TChannel &operator=(const TChannel &b)
	{
		if (this == &b)
			return (*this);
		clear();
		width = b.width;
		height = b.height;
		depth = b.depth;
		data = b.data;
		return (*this);
	}


	T operator[](uint32_t i) const
	{ return data[i]; }

	T &operator[](uint32_t i)
	{ return data.at(i); }

};

typedef TChannel<uint8_t> Channel;
