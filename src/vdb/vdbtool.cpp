#include "vdbtool.h"


void Help()
{
	String help = R"xxx(
Parser1 options are:
-h					:this help
-i filenames list	:load image sequence, process it, write out VDB
)xxx";
	std::cout << help << std::endl;
}


auto processFile(String &fname)
{
	// read
	auto cntr = 0;
	Timer timer;
	timer.start();
	auto img = Image(fname);
	auto msg = fmt::format("file '{}' processed", fname);
	timer.stop(msg);
	return img;
}


auto processFiles(std::vector<String> &lst)
{
	std::cout << "processFiles(): got list of files:\n";
	for (auto &f: lst)
		std::cout << f << std::endl;
	auto cntr = 0;
	auto sz = lst.size();
	Stack res;
	for (auto f: lst) {
		Timer timer(fmt::format("Processing [{}/{}]: '{}'", cntr, sz, f));
		auto stack = processFile(f);
		auto msg = String("done.");
		timer.stop(msg);
		cntr++;
		res += stack;
	}
	// placeholder for return value, change it later
	return res;
}


auto ArgsRouter(int argc, char **argv)
{
	/* Routing hub for actions (depending on arguments)
	 * Return list of args or display help
	 *
	 */
	std::vector<String> res;
	Args arg(argc, argv);
	if (!arg.getCmdNum() || arg.cmdOptionExists("-h")) {
		Help();
	} else if (arg.getCmdNum() && !arg.cmdOptionExists("-h") && !arg.cmdOptionExists("-f")) {
		// get all args
		res = arg.getAll();
	} else if (arg.cmdOptionExists("-f")) {
		// read and return filelist
		String flistfile = arg.getCmdOption("-f");
		std::ifstream f(flistfile);
		String s;
		while (std::getline(f, s))
			res.push_back(s);
	}
	// debug
	//res.clear();
	//res.push_back(String("C:/_vdb/tread/refs/ref.tif"));
	return res;
}


int main(int argc, char **argv)
{
	// pdcurses
	auto win = initscr();
	cbreak();
	scrollok(win, true);
	// openvdb
	openvdb::initialize();
	// args
	auto args = ArgsRouter(argc, argv);
	String fout = "out.vdb";
	/*if (args.size() > 1)
	{
		fout = args.back();
		args.pop_back();
	}*/

	// Start here:
	Stack stack(args);
	if (!stack.depth()) {
		std::cout << "empty Stack() detected: " << stack << std::endl;
		return 0;
	}
	auto msg = fmt::format("Minmaxing {}\n", stack);
	printw(msg.c_str());
	refresh();
	auto mnx = stack.minmax();
	auto den_min = mnx[0];
	auto den_max = mnx[1];
	auto mx = stack.width();
	auto my = stack.height();
	auto mz = stack.depth();

	msg = fmt::format("stack {}: minmax is {}-{}\n", stack.depth(), mnx[0], mnx[1]);
	printw(msg.c_str());
	refresh();
	msg = fmt::format("Writing out vdb file: {}\n", fout);
	refresh();
	stack.write_vdb(fout);
	msg = fmt::format("Writing out vdb file: {}\n", fout);
	refresh();
	//printw("Press any key to continue.."); getch();
	endwin();
	return 0;
}

