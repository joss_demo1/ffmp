#pragma once

#include "headers.h"

class Args
{
public:
	Args(int &argc, char **argv)
	{
		for (int i = 1; i < argc; ++i)
			_tokens.push_back(std::string(argv[i]));
	}

	std::string getCmdOption(std::string &option) const
	{
		std::string result("");
		std::vector<std::string>::const_iterator it;
		it = std::find(_tokens.begin(), _tokens.end(), option);
		if (it != _tokens.end() && ++it != _tokens.end()) {
			return _tokens[it - _tokens.begin()];
		}
		return result;
	}

	std::string getCmdOption(const char *option) const
	{
		std::string result("");
		auto str = std::string(option);
		std::vector<std::string>::const_iterator it;
		it = std::find(_tokens.begin(), _tokens.end(), str);
		if (it != _tokens.end() && ++it != _tokens.end()) {
			return _tokens[it - _tokens.begin()];
		}
		return result;
	}

	bool cmdOptionExists(std::string &option) const
	{
		return std::find(_tokens.begin(), _tokens.end(), option) != _tokens.end();
	}

	bool cmdOptionExists(const char *option) const
	{
		auto str = std::string(option);
		return std::find(_tokens.begin(), _tokens.end(), str) != _tokens.end();
	}

	int getCmdNum(void) const
	{
		return static_cast<int>(_tokens.size());
	}

	std::vector<std::string> getAll(void) const
	{
		return _tokens;
	}

private:
	std::vector<std::string> _tokens;
};
