#pragma once

// Args
#include <stdarg.h>
// General
#include <iostream>
#include <fstream>
#include <sstream>
#include <istream>
#include <ostream>
#include <ios>
//
#include <string>
#include <vector>
#include <set>
#include <regex>
#include <filesystem>
#include <cstdint>
#include <algorithm>

#define _USE_MATH_DEFINES
#include <cmath>

#ifndef COUT_SET
#define COUT_SET {cout.setf(std::ios::fixed); cout.precision(8);}
#define OSTR_SET {ostr.setf(std::ios::fixed); ostr.precision(8);}
#endif


// Libs
#include "omp.h"
#include "re2/re2.h"
#include "fmt/format.h"

// OIIO
#include <OpenImageIO/imageio.h>

// OpenVDB
#ifndef _OPENVDB_DISABLE_SPAM
#pragma warning(disable:4146)    // unary minus operator applied to unsigned type, result still unsigned
#pragma warning(disable:4251)    // 'openvdb::v6_1::math::ScaleMap::mScaleValues': class 'openvdb::v6_1::math::Vec3<double>' needs to have dll-interface to be used by clients of class 'openvdb::v6_1::math::ScaleMap'
#pragma warning(disable:4267)    // 'argument': conversion from 'size_t' to 'long', possible loss of data
#pragma warning(disable:4244)    // 'argument': conversion from 'int64_t' to 'long', possible loss of data
#pragma warning(disable:4275)    // non dll-interface class 'std::exception' used as base for dll-interface class 'openvdb::v6_1::Exception'
#pragma warning(disable:4996)    // 'argument': conversion from 'int64_t' to 'long', possible loss of data
#define _OPENVDB_DISABLE_SPAM
#endif
#include <openvdb/openvdb.h>
#include "openvdb/tools/dense.h"
#include <OpenImageIO/imageio.h>
#define INCLUDE_WINDOWS_H
//#undef MOUSE_MOVED
//#include "curses.h"

/**/
// Qt
#define QT_NO_KEYWORDS
#include <Qt>
#include <QtCore/QDebug>
#include <QtCore/QEvent>
#include <QtCore/QElapsedTimer>
#include <QtCore/QVector>
// QtGui
#include <QtGui/QCursor>
#include <QtGui/QKeyEvent>
#include <QtGui/QMouseEvent>
#include <QtGui/QVector3D>
#include <QtGui/QQuaternion>
#include <QtGui/QGenericMatrix>
#include <QtGui/QMatrix4x4>
#include <QtGui/QOpenGLFunctions>
// get all widgets at once
#include <QtWidgets/QtWidgets>
#include <QtOpenGLWidgets/qopenglwidget.h>
// Concurrency and others
#include <QtConcurrent/QtConcurrent>
#include <QSet>
// Qt OpenGL
#include <QtOpenGL/qopenglfunctions_4_5_core.h>
#include <QtOpenGL/QtOpenGL>
//#include <Qt3DCore>
/**/




// Cast native types
typedef uint8_t U8;
typedef uint16_t U16;
typedef uint32_t U32;
typedef uint64_t U64;

typedef int8_t I8;
typedef int16_t I16;
typedef int32_t I32;
typedef int64_t I64;

typedef U8 Byte;
typedef std::vector<Byte> Bytes;
typedef I32 Int;
typedef I64 Lint;
typedef float Float;
typedef double Double;
typedef Float Real;
typedef std::string String;
typedef std::vector<String> Strings;
