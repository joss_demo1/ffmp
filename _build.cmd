@echo off
echo ------------------------------------------------------------------
set bd=%~dp0build
if EXIST %bd% (
	::echo Removing %bd%
	::rd /s /q %bd%
)
mkdir %bd%
cd build
:: set DESTDIR=C:\Programs\Lib
FOR /F "tokens=*" %%g IN ('where python') do (SET PY=%%g)
echo %PY%
cmake ../ -G "Visual Studio 17 2022" "-DCMAKE_TOOLCHAIN_FILE=C:/Users/joss1/Vcpkg/scripts/buildsystems/vcpkg.cmake" "-DVCPKG_TARGET_TRIPLET=x64-windows" "-DCMAKE_INSTALL_PREFIX=./install" "-DPython3_FIND_STRATEGY=LOCATION" "-DPython3_EXECUTABLE=%PY%" --trace-source=CMakeLists.txt && cmake --build . --target ALL_BUILD --config Release && pause && cmake -P cmake_install.cmake
::  && cmake -P get_runtime_deps.cmake
:: "-DCMAKE_BUILD_TYPE=Release"
:: --log-level INFO