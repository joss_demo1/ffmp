:: base
vcpkg install boost fmt pthreads pystring python3 re2 tiff yaml-cpp zlib pkgconf
:: 3d prog
vcpkg install freeglut glm opencv4 libnoise openmesh openvdb lcms ffmpeg
:: EXR
vcpkg install ilmbase openexr opencolorio openimageio 
:: 3d
vcpkg install alembic
:: qt
vcpkg install qtbase qtdoc qtimageformats qtmultimedia qtsensors qtshadertools qttools qttranslations
:: AI
:: vcpkg install libtorch
:: USD - TBB is broken
:: vcpkg install usd



