@echo off
cls

set bd=%~dp0build
if EXIST %bd% echo Removing %bd% && rd /s /q %bd%
python setup.py build_ext
