# FFMP:  

#### A Python module setup, using vcpkg infrastructure to build ffmpeg-enabled Python C++ extension.
=====================================================================================================

## What is it?

  - This is a Proof of Concept app containing few simple but important aspects:
    - This is a single `ffmp.pyd` file that can be imported just like any other Python module: `import ffmp`
    - It contains a few simple tools each using some external library:
      - Camera grabber, 2 versions
      - Screen grabber
      - Sample C++ Qt GUI
      - VDB Tool
      - Image loader
    - The idea is to embed powerful third-party libraries into a simple Python tool (not just a C++ app) to achieve "complex" things like realtime screen / camera grabbing, OpenCV recognition, AI inference out of the box inside of VFX DCC app.

    That's basically a boilerplate app that can quickly be converted into a full fledged Python extension.
    With the wide spectrum of libraries available with Vcpkg now it's piece of cake to cook these apps.

## Use cases:

	- TCP/UDP server as a Python module reading your phone camera data stream in realtime in Maya or Unreal Engine (real case).
	- Screen capture, stills and video for collaboration apps
	-

	Things like video encoding and screen capturing from a Python extension become achievable without resorting to external tools or shell commands.
	Python module setup allows to use Cmake/Vcpkg to build module in-place on user workstation.

  - It's a proof of concept of video recording / image manupulation tool written in C++ and working as a Python extension

	Here's the idea is to extend it to a full-featured lightweight and customizable command-line image processor that's able to perform a range of image tasks
	from simple image transformations to config-based slate overlaying for Deadline	processing (for VFX purposes).
	With a broad range of existing open source libraries, image manipulations turns	into a piece of cake and this is sort of a demo.

  - OCIO/OIIO/OpenCV/FFMpeg testbed for aforementioned purposes:

	Full color pipeline support testbed.

  - Python interface:

	Another intent is to experiment with different ways of exposing C++ to Python interfaces:
	native Python, PyBind11, Boost-Python, SWIG.

## Usage:

```
  - get MSVS 2022 and Vcpkg
  - git clone
  - _reqs.cmd
  - fix path to vcpkg in _build.cmd if needed
  - _setup.cmd
  - _test.cmd
  - >>import ffmp
  - >>ffmp.scr()
  - >>ffmp.cam()
  - >>ffmp.qt()
```

## Plans:

- Video decoder/encoder tool, sequence-to-images and back
- Desktop video recorder tool
- Screenshot tool
- C++/Python QImage interoperability, image manipulations
- Config-based slates system: draw on top of video with the system above
- VDB video stacker tool: stack video frames into a OpenVDB volume

## Random links:

- [] [Interfacing with C: SciPy](http://scipy-lectures.org/advanced/interfacing_with_c/interfacing_with_c.html)
