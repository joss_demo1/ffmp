import os
import logging
import sys
from pathlib import Path


logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - "%(name)s" (%(filename)s:%(lineno)d), %(levelname)s\t:"%(message)s"')
LOG = logging.getLogger(__name__)

# Env setup
ROOT = Path(__file__).parent.parent.resolve()
TEST = ROOT / 'test/seq'
BUILD = ROOT / 'build'
INSTALL = BUILD / 'install'
BIN = INSTALL / 'bin'

LOG.info(f'ROOT: "{ROOT}"')
LOG.info(f'BUILD "{BUILD}"')
LOG.info(f'INSTALL "{INSTALL}"')
LOG.info(f'BIN "{BIN}"')

if not str(BIN) in sys.path:
	LOG.info(f'Add "{BIN}" to sys.path')
	sys.path.append(str(BIN))

qt_debug = 'QT_DEBUG_PLUGINS'
if not qt_debug in os.environ:
	LOG.info(f'Add "{qt_debug}" to os.environ')
	os.environ[qt_debug] = '1'

qt_plugins_path = 'QT_QPA_PLATFORM_PLUGIN_PATH'
if not qt_plugins_path in os.environ:
	LOG.info(f'Add "{qt_plugins_path}" to os.environ')
	os.environ[qt_plugins_path] = str(INSTALL/'plugins/platform')


import ffmp


def test():
	files = [str(x).replace('\\','/') for x in TEST.glob('*.jpg')]
	LOG.debug(files)
	r = ffmp.vdb(files)
	print(r)

if __name__ == '__main__':
	test()
