set "OUTDIR=%~dp0\seq"
if NOT EXIST %OUTDIR% echo Creating %OUTDIR% && mkdir %OUTDIR%
ffmpeg -i "%1" -vf "select=not(mod(n\,10))" -vsync vfr "seq/out.%04d.jpg"